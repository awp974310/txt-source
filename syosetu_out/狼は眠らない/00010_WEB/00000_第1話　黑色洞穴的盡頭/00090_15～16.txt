１５

「喂！」

料理長的莫爾德把布袋遞了過來。
袋子裡裝滿了格里菲的葉子。

「感謝！」

道完謝後，雷肯拿起了一片葉子。隨意的丟進嘴裡，大口的嚼著。苦澀的味道在口中蔓延開來。是沒有完全曬乾嗎？尖銳味道的刺激著舌頭。但這樣也好。食不知味，這正好就是雷肯現在的狀態。

做什麼都打不起精神。
而且是這個狀態的人並不只有雷肯一個。小姐不在的第二天劍的練習也突然就結束了。傭人們也一個個的都不說話。

雷肯一天到晚老是坐在樹蔭下的石頭上，看著種得整齊漂亮的蔬菜。右邊的石頭是莫爾德的老位置。
兩人並沒有交談，只是嘴裡嘟嘟噥噥的。

雷肯感到了一絲寂寞。
這份寂寞究竟是重要的寶玉去到了遠方的原因，還是露比安菲露小姐去到了遠方的原因呢？雷肯自己也分不清楚。

咔嚓咔嚓的品嘗著買下來的高價嗜好品，雷肯思考著那隻巨大魔獸的事情。
那隻怪物和雷肯的相性不合。去掉相性這點也不是能夠獨自一人解決掉的對手，即使再去掉獨自一人這點也很難一戰。

雷肯擅長的是，在狹小的地方以一敵多的戰鬥。再迷宮裡面對魔獸或是在戰場上獨自一人被包圍，這時雷肯就能最大限度的發揮他的能力。
大概那個怪物額頭上的第三隻眼睛能夠看到魔力，所以才能透過障礙物感知到雷肯的存在。
〈立體知覺〉的有效範圍是半徑五十步，以那個怪物為對手來說有點半生不熟。對方在五十步以上的攻擊方式至少也有兩種以上。除此之外還有對那攻擊範圍和速度都沒有應對之策的滾動攻擊。

怪物的表皮估計也很硬，雷肯的攻擊也不知道能不能有用。
使用＜爆裂彈＞的話說不定能夠做點什麼，但那是緊要關頭才會使用的珍藏道具，一旦使用了就沒辦法再補充。

要是波德在的話就好了。
對這種表皮堅硬的魔獸，波德使用的鈍器和他的能力＜貫通衝擊＞十分有效。雷肯負責吸引魔獸注意力，波德負責攻擊，如此一來那個魔獸也就沒什麼好怕的了。

但是波德並不在，其他能夠聯手的人也沒有。
只能自己一個人來。

總之再交手一次，先觀察一下它的能力。其他的事情就這之後再說吧。
雷肯把沒有了味道的葉子從嘴裡吐出後站起身來。


１６

第二天早早就出門的雷肯在深夜時分回來了。

今天的戰鬥真是千鈞一髮。沒能躲開怪物的刺，雷肯肚子上被開了個大洞，多虧身上帶著的上級治癒藥水才撿回一命。
魔法藥的問題也是讓人頭痛。

本來下級和中級的治癒藥水還有魔力恢復藥水都是雷肯自己製作，但是在這個世界既不知道要用什麼材料也不知道製作的方法。有必要找一個優秀的藥劑師，讓他教授一下製作方法才行。這樣下去根本沒辦法進行一些危險的戰鬥。上級藥水也必須得弄到手，如果有的話。

不管怎麼說今天的戰鬥還是很有意義的，怪物的手牌已經弄清楚了。

首先是怪物將身上的刺射出的攻擊，只要刺還有就能夠進行連發，但是距離超過五十步以後威力就會下降。
其次冰霜吐息的有效範圍大概是七十步到百步左右的距離，在五十步以內連大樹的芯都能凍結，但是距離超出百步以上就只有凍住葉子程度的威力。
最後是十分具有威脅的滾動攻擊，雖然在滾動狀態下怪物的皮膚會更加硬化，物理攻擊完全沒用。但是滾動的速度並沒有想像中的快。在森林裡姑且不論，只要在寬闊的地方雷肯就能逃掉。

雷肯先前預想的「相性不合，獨自一人無法打倒」，在現在看來好像並不是如此。

以集團等大規模人數去討伐的話，全體的移動速度會很遲緩，一個滾動攻擊就會全滅。能夠進行遠距離攻擊的魔法師在進入百步範圍以內的話就會有冰霜吐息在等著，而且能在百步以外的距離進行魔法攻擊的人也很少。就算湊齊這種魔法師，也沒有能夠阻止魔獸腳步的方法，在被接近到百步以內大家都得死。

但是雷肯獨自一人的話就能使用＜突風＞的能力進行加速，一邊逃脫一邊消耗魔獸的魔力。
話雖如此，攻擊卻是個大問題。
用劍連續砍中頸部三次也沒有什麼效果。

不是砍向背上和兩邊的頸部，而是從喉嚨下方往上砍過去的話大概能夠造成更大的傷害。
但是要在那麼低的位置瞄準那嬌小的頭部，還要從喉嚨下方往上砍，是非常困難的。

完全想不出解決方案。只能在戰鬥中找到一條活路了。
決戰越早越好。時間拖得越久就越難打倒那個怪物。