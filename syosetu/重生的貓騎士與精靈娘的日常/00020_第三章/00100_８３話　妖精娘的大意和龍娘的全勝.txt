「接下來會出現怎樣的敵人呢~？」
「哼哼，沒有兩下子的敵人可不是我們的對手哇！」


菲莉一邊抱著塔瑪一邊往前走。
另一邊，趴在塔瑪背上享受著毛茸茸觸感的莉莉也一副雀躍的樣子。

果然，在氛圍和之前住過的完全不一樣的陌生迷宮裡冒險，她們二人都樂此不疲。


而跟在二人身後的薇兒卡自不用說，已經調整過來的艾麗婭和史黛拉也一邊警戒著周圍，一邊往前進。


(呼呣。莉莉和菲莉自身的實力已經確認過了，那麼就在這裡給大家上一下加護吧)


被菲莉抱著的塔瑪「喵！」的發出了一聲可愛的叫聲。
下一瞬，一行人都被黃金色光芒溫柔地包裹起來。
這是塔瑪發動了固有技能《獅子王的加護》，給眾人賦予了加護。


「這、這是啥！？」
「攻擊力和防禦力……各種狀態值都被強化了~！」


自己的身體突然被光芒包裹住，而且狀態值也蹭蹭地往上漲，對此莉莉和菲莉都感到十分驚訝。


「呵呵，莉莉醬和菲莉醬也感到很驚訝吧？」
「呼哈哈哈哈！吾一開始亦如此！」
「莉莉醬、菲莉醬，這是塔瑪醬使用的buff技能帶來的效果喵！」


看到驚訝的莉莉和菲莉，艾麗婭小聲地笑著，史黛拉一臉讚同，而薇兒卡則作出了說明。


「好厲害哇！塔瑪不僅可愛又強，連這種事都做得到呢！」
「明明還這麼小隻，塔瑪真是隻乖乖喵~！」

知道塔瑪為了她們使用了buff技能，莉莉和菲莉感到非常開心。
於是莉莉更加埋頭撫摸，菲莉則對塔瑪進行摸頭殺。


「喵~~~！？」


被兩個妖精族圍攻，塔瑪發出了困擾的叫聲。


「……！好像有腳步聲！」
「真的喵！莉莉醬、菲莉醬，作戰鬥準備喵！」
「了解哇！」
「這次也請交給我們吧~！」


艾麗婭聽到敵人的腳步聲後提醒眾人。
薇兒卡則對還在對塔瑪上下其手的莉莉和菲莉作出指示。
而史黛拉則露出稍微有點不安的表情，「唉，尚未輪到吾出場嗎……」喃喃道。


『哞哦哦哦……』

隨著叫聲出現的是牛人型的魔物——C+級的米諾陶諾斯。


這次是兩頭一起。
它們手上都握著戰斧，閃著光芒的眼睛緊緊地盯著艾麗婭她們。
不知道是因為想著有美味的食物送上門，還是因為看到艾麗婭她們美麗的身姿而感到興奮……。


「你們要小心一些！米諾陶諾斯比哥布林、獸人之類的要聰明多了哦！」
「偶爾還會出現能使用魔法技能的個體，一定要多加注意喵！」


對進入了戰鬥狀態的兩位妖精族少女，艾麗婭和薇兒卡都出言提醒。

「所以說要先手制勝喲，菲莉！」
「一起上吧~！」


言畢，莉莉和菲莉同時發動了技能。


「《妖精彈》！」
「《樹鞭》~！」

莉莉和菲莉使出的，是和剛才一樣的光彈和樹鞭。


咚嘭——！


隨著巨大的響聲發出，下一瞬間，其中一頭米諾陶諾斯發出『哞哦哦哦哦哦……！？』苦悶的叫聲。
它捂住自己的腹部，而鮮血正源源不斷地從其指間溢出。

「好、好厲害！《妖精彈》竟然直接貫穿了那頭看上去硬邦邦的魔物！」


莉莉也情不自禁地驚訝道。


那是當然的。
本來《妖精彈》自身就不是一種能給予強力打擊的大威力技能。
但是，現在卻直接貫穿了米諾陶諾斯那有如鎧甲般堅硬肌肉保護著的腹部。


「那就是塔瑪給予的buff技能的力量嗎……那麼我也要上了喲~！」


目睹莉莉那經過強化後的力量後，這次輪到菲莉操作起她的《樹鞭》了。
隨即，那伸長到幾乎要頂到天花板的大樹枝便向剩下的那頭米諾陶諾斯頭頂甩了下去。


『卟哞……！』


米諾陶諾斯連忙向一旁閃避——並沒有這麼做，而是出乎意料地將戰斧架到腰旁，接著便向甩下來的《樹鞭》猛然揮出戰斧。


雖然經過了攻擊力強化，但畢竟《樹鞭》是木製的。
在鐵製的戰斧，再加上米諾陶諾斯臂力的合力下，樹鞭將會被一斧兩斷……。


在場的所有人都這麼想的。
但只有一人，那就是菲莉除外……。


「呼呼~，現在的只是障眼法~！再來一根，《樹鞭》~！」


就在米諾陶諾斯即將要砍斷《樹鞭》時，菲莉小小地壞笑了起來，接著她從腳下又召喚出了一根新的《樹鞭》。

新的《樹鞭》「咻啪！」地帶著破風聲彈射而出。
使出了大動作攻擊的米諾陶諾斯顯然對這突然而來的攻擊毫無迎擊之力。
隨即，《樹鞭》尖銳的枝頭貫穿了它的左胸。

『卟……哞……！？』

米諾陶諾斯一邊口吐鮮血，一邊發出了難以置信的聲音。
它眼中的光芒逐漸消散，身軀也慢慢地倒在了地上。


與此同時，以莉莉為對手的米諾陶諾斯也緩緩向地面倒去。

「做到了呢，菲莉！」
「成功了莉莉醬！這也多虧了塔瑪醬呢~！」


打倒了兩頭米諾陶諾斯，莉莉啪嗒啪嗒地拍打著翅膀飛向菲莉，二人高舉手相互擊掌。


正在此時……。


——史黛拉！
——明白！

塔瑪急忙向史黛拉送去念話。
而史黛拉立馬點了點頭，架起巨盾向莉莉和菲莉的方向疾馳而去。


下一瞬間——


咚轟！


傳來了一聲爆炸聲。
史黛拉的巨盾和敵人放出的《火球》發生了激烈的衝突。

「真是，明明已多次告誡不可大意！」

一邊說著，史黛拉一邊揮舞起巨劍，向前方突擊。
雖然並沒有龍人化，但有塔瑪《獅子王的加護》的加成，她的速度也是非同尋常。


「最後一擊！」


劍之所指，乃是莉莉的對手，那頭向地上倒去的米諾陶諾斯。
接著，史黛拉向著它的腦袋揮下了巨劍，這下它才真正的被打倒了。


「竟、竟然……剛剛是裝死的嗎……？」
「史黛拉醬，謝謝你~！」


莉莉感到十分驚訝，菲莉則向史黛拉送上感謝的話語。

正如莉莉所言，剛剛其中一頭米諾陶諾斯在裝死。
它看準莉莉放鬆大意的那瞬間，發動了魔法技能《火球》。


「好厲害，史黛拉醬！」
「是喵！比我們的反應快多了喵！」


同樣從後方疾馳而來的艾麗婭和薇兒卡也對史黛拉讚不絕口。


當然了，她們也察覺到米諾陶諾斯的動作而作出了反應，但遠比史黛拉的反應要來的遲。
經過在格拉德斯通的鍛鍊，史黛拉作為優秀的肉盾確實有了顯著的成長。


「哼，莉莉和菲莉戰鬥之術實為稚嫩，吾看不下去而已！」


被大家誇獎而感到十分難為情的史黛拉，滿臉通紅地說道。


——史黛拉，做得不錯嘛。
——……！塔瑪稱讚吾了！

在眾人的稱讚中聽到塔瑪的念話，史黛拉表情瞬間明亮了起來。
被愛著的異性——塔瑪稱讚，就算是傲嬌重患的史黛拉也被一發入魂了。


心情突然變得極好的史黛拉，臉上帶著笑容看向塔瑪，而另一邊的塔瑪則點了點頭。
對此，艾麗婭她們四人都「「「「…………？」」」」地帶著一臉不可思議的表情看著這光景。
