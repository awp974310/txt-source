Epilogue

逃離開始崩潰的遺迹的艾特等人，成功瞞過在入口附近待機的帝國後方支援部隊的眼線，平安回到了港口都市巴努亞斯。
「啊，是哥哥你們呀！」
走進旅館後，梅洛蒂便跑了過來。
艾特抱住她問道。
「壹個人寂不寂寞啊？」
「不寂寞，有大家陪我玩！」
梅洛蒂看向了在旅館工作的那些負責接待的姐姐們。看到兄妹再會，大家都露出了開朗的笑容。
艾特向她們低下頭，
「真的很感謝各位幫忙照顧舍妹」
「沒關系的，我們都習慣了」
聽到接待員代表的大姐姐的回答，艾特打從心底覺得能找到這麽家好旅館真是太好了。
「啊，對了！」
像是想起什麽而出聲的梅洛蒂，向芭妮拉抛去了鬧别扭的視線，
「芭妮拉也和哥哥在壹起嗎？明明說好會陪我的」
「啊哈哈，抱歉我擅自走掉了」
芭妮拉雙手合十道起了歉。
「我剛好有點事，跟艾特是剛剛才碰到的」
在走進旅館前，艾特對芭妮拉說，不能讓梅洛蒂發現他們曾對立過。這是為了不讓梅洛蒂多余的心。
并且還告訴她要用平時的態度面對他們。
「你能原諒我嗎？」
「梅洛蒂，想吃甜食！」
「诶？」
「請我吃甜食，就原諒你」
聽完這句話，芭妮拉的表情壹下子亮了起來。
「這樣的話，你想吃多少都行」
這下就和好了吧，艾特在心裏松了口氣。
（話說回來，真是累死了......）
氣力和體力都已經到極限了。因此在說明自己想要早點休息之後，等他們回來已經等得不耐煩的梅洛蒂還抱怨想要多和哥哥玩壹會兒。
惹她不高興了。
不過在晚飯過後，芭妮拉按照約定請她吃甜甜的聖代之後，她的心情就立馬變好了。不過，似乎還有壹個條件。
「哥哥，今天能和我壹起睡嗎？」
「我可是特意給你備了張床啊？」
「可是，人家想壹起睡嘛......好嗎？」
從剛剛開始她就壹直纏着自己，撒嬌撒個沒完。嘴上說不寂寞，那肯定只是在逞強吧。
「好吧。壹起睡吧」
「诶嘿嘿......梅洛蒂好開心呀❤」
看着緊緊抱住艾特的梅洛蒂，
「呵呵呵，真是兄妹的二人世界呢。那麽，我就去睡空床吧」
這個房間裏只有兩張床，所以維諾斯原本打算在艾特體内睡覺，但空出來了壹張，她似乎打算去睡那裏。
跟她說得壹樣，吃完晚飯回到房間的維諾斯壹下子紮在床上，壹副十分滿足的模樣。
順帶壹提，艾特他們定了三間房間，艾特和梅洛蒂還有維諾斯壹間，莉莉希娅壹間，芭妮拉壹間。
「那，我先去洗澡了。這座旅館的地下有大浴場，聽說對恢復疲勞很有幫助」
「是嗎。聽你這麽壹說，我也想去洗了呢」
「那你就和梅洛蒂壹起去吧。把莉莉希娅跟芭妮拉也帶上」
「這麽說，溫泉是男女分開的咯？」
「這不廢話嗎。好了，我走了」
艾特當即便離開房間，前往了大浴場。


☆  ☆  ☆


「啊——，真爽......」
獨自泡在巨大的浴池中，艾特不由自主地說道。
這只能用復活來形容。甚至讓人覺得如果早知道這麽舒服，在去探索之前就應該先來泡壹次的。
正在他覺得壹直泡在池子裏，感覺能直接睡着的時候，傳來了咔啦咔啦的開門聲。
本以為應該有是有其他客人進來了，
「艾特，艾特」
「什——」
可叫他的聲音，令他吓了壹大跳。
扭過頭去後正如他所料，聲音的來源者芭妮拉正站在那裏。
她壹絲不挂，身體的凹凸，柔軟的曲線，下半身整齊又稀疏的草叢全都看得壹清二楚。
「喂，這裏可是男澡堂啊！為毛你都脫光了啊！」
「噓，安靜點。維諾斯和梅洛蒂妹妹正在隔壁女澡堂泡澡呢。被聽到會很不妙吧？」
「就是說，你是明知故犯跑來找我的嗎？」
「沒錯。從維諾斯那裏聽說後，我就拜托她了。讓我和艾特壹起洗澡❤」
嘩啦壹聲，芭妮拉進入了浴池。
随後艾特在心中呼叫着維諾斯。
『維諾斯，聽得到不？』
『聽得到。我正想找你呢』
『你到底是何居心啊』
『這個嘛，就跟你想得壹樣啊。我沒告訴莉莉希娅洗澡的事所以放心吧，現在在洗澡的只有我跟梅洛蒂——雖然很可惜，不過我們馬上就出去了。我會哄梅洛蒂睡覺的，你就好好享受和芭妮拉的幽會吧。這對我來說也是件好事呢』
『喂，維諾斯！』
再怎麽叫也沒有回答，她好像也不打算回應了。
「——那家夥，真是服了」
聽到艾特的嘟囔，應該察覺到了什麽。
「你在和維諾斯說話啊嗎？」
「她說馬上就出去了」
「說起來，艾特和莉莉希娅壹起洗過澡嗎？」
「這，倒是還沒有......」
順便壹提，和梅洛蒂倒是已經洗過不知道多少次了。梅洛蒂和莉莉希娅雖然也和莉莉希娅洗過，但并沒有三人壹起，以及和莉莉希娅單獨洗過澡。
「太好了！那麽在這方面，就是我領先了呢❤」
開心地露出微笑的芭妮拉将肩膀靠了過來。
「那麽，艾特。你應該已經知道，我是來做什麽的吧？」
「呃，嗯」
肯定，是關于眷屬的事吧。
「那就拜托你了。讓我和莉莉希娅壹樣——。按照約定，成為艾特的眷屬......」
「真的可以嗎？」
「嗯，可以」
手手相疊，芭妮拉将手指纏了過來。
「......我知道了。我會兌現諾言的」
艾特凝視着芭妮拉的臉龐，
（剛相遇時，根本沒想過會這樣啊）
那時她看上去就像是個男孩子，但現在已經不是了。染上壹絲绯紅的臉頰，還有濕潤的眼眸都十分有魅力，她已經成長為了壹名會讓人想要抱她的女孩。
凝視着她的眼睛，艾特問道。
「芭妮拉，你願意成為我的眷屬嗎？」
「嗯，我願意。我願意成為艾特的眷屬」
在浴池之中，二人互吻。
眼前染成壹片純白，然後看到了熟悉的圖案。
撲通，艾特的心臟用力跳了壹下。
「怎麽樣？」
「和跟阿莎蕾娅契約時感覺壹樣。我看到了維諾斯肚臍下方的那個圖案。這下，我就成為眷屬了嗎？」
「我也看到了，應該是成功了」
「好開心❤」
随後，二人又接了壹吻。
那是舌頭互相纏綿的冗長壹吻。
在吻完之後，
「後面的事，肯定也要做吧......？」
這麽說着，芭妮拉握住了在艾特的下半身膨脹聳立，主張自己存在的那個東西。
他擔心是不是又像莉莉希娅那時失控了而看向胸口，但那裏并沒有出現斑紋。也就是說，會變成這樣是出于他自己的意識。
「你和莉莉希娅做過的事，我也要做壹遍。這樣壹來，就能變得更強對吧？」
「......嗯」
順便壹提維諾斯也會被強化，而從結果而言這個行為也會令艾特更強。他沒有不做的選項，況且剛才維諾斯也明确要求他做了。
送到嘴邊的肥肉不吃枉為男人，之前也這麽說過。
（如果有别人進來了，用魔眼處理就行了）
再說在旅館和食堂都沒見過其他男性住客，人數應該并不多。
肯定沒問題吧。
「那麽，我要壹口氣追上莉莉希娅❤不對，我要超過她。艾特，你在那裏坐好❤」
芭妮拉視線所指的，是清洗身體時用的椅子。
還以為她想做什麽，看來事情很簡單，她是想給艾特洗身子。
「怎麽樣？被這麽服務，長大之後應該還是頭壹次吧？」
芭妮拉用海綿像小孩子壹樣為他清洗着身體。她說得沒錯，雖然有點難為情，不過感覺确實不壞。
（挺舒服啊）
甚至讓他有種自己是王的感覺。
雖然不及莉莉希娅，但芭妮拉的胸部也相當豐滿，抵到背上的感覺，自然是妙極了、
（——啊，對了）
就在這時他突然想起壹件事。
讓芭妮拉用奶子來洗身子應該會更舒服，還能感受壹下當王的感覺啊。
「我說，芭妮拉。能用你的胸部代替海綿，幫我洗背嗎？」
「诶？怎麽突然——」
「現在光是抵在背上就很舒服了，要是那麽做的話，我覺得應該會更舒服」
「是，是嗎？那，我就試試——這樣可以嗎？」
用肥皂在胸口打起泡沫後，她用胸部在艾特的後背上滑來滑去。
「啊啊，爽爆了」

(圖片013)

觸感比想象中還要舒服。
「那我就繼續咯」
滑啊滑，滑啊滑，在芭妮拉不斷用胸部洗背的時候。
「哎，艾特。這裏也幫你洗壹下吧？」
她這麽說着，将手伸向了艾特腫大的下半身的某個部位。
握住那裏後，她動起了手。
「看來很舒服呢，不用問就能感覺出來」
「呃，嗯」
老實講，爽到飛起。都讓人想要壹直這麽做下去了。
但是，繼續讓她弄的話要不了多久就會迎來極限。覺得比芭妮拉先去會顯得很遜的艾特，決定攻守互換。
「接下來，輪到我幫你洗了」
「嗚诶！？」
心想不能壹直被動的艾特站起身，和芭妮拉交換讓她坐在了椅子上。不過，就算用男人硬邦邦的身體給她洗，應該也不會舒服。
當然他也壓根沒打算怎麽做。
他的做法，是将自己的手放在芭妮拉柔嫩的肌膚上，來回摩擦為她清洗。
「喂，艾特！你在做什——嗯！那裏不行——呀啊，啊啊啊嗯❤」
她最後會發出那種聲音，是因為艾特開始玩弄她的乳頭。
芭妮拉那裏好像很敏感，她的快感看上去十分明顯。
摸着摸着，可愛的淡粉色乳暈頂端的小豆豆，便逐漸腫脹起來。
「等等，都說了不行啦，你怎麽玩弄的話......！」
「既然如此，那我換這裏洗吧？」
接着，艾特将手伸向了下半身。
「诶，那裏更不行了......啊啊啊啊啊啊啊啊啊啊啊啊！！」
光是用手指擺弄入口，她似乎就相當有感覺。從扭動着身子的芭妮拉體内，溢出了大量的蜜汁。
正當他想接着将手指插進去攪弄内部時，
「哈，啊......不行，艾特......不要，再繼續了......我已經，忍不住了」
轉過頭，面紅耳赤的芭妮拉如此懇求道。
逗弄着她的耳朵，艾特問道。
「你的意思是，想讓我進來嗎？」
「......嗯」
芭妮拉點點頭。
艾特自然沒有異議。
因為他也想插進去想得不得了。
「好。那你坐上來吧」
看着艾特腫脹變大的那活兒，芭妮拉咽了口口水。
接着，她正面坐在了艾特的身上。
「......那，我放進來咯」
芭妮拉自己對準位置，沉下了腰。
「哈啊，唔......！」
是因為痛嗎，她壹臉難受的表情咬緊了牙關。
但即便如此，她仍沒有停下腰部的動作。
緊接着，艾特用敏感的部位感受到了捅破什麽東西的感覺。
「剛才難道，你......」
「......幹嘛啊？」
「沒什麽，就是在想你不會是處女吧」
「什麽意思？你覺得我不是嗎？」
如此回答的芭妮拉，眼中噙着淚水。這不像是演技，前端的觸感也不會撒謊吧。
而且，在二人的結合部位還流出了紅色的東西。
她毫無疑問是處女。
「剛剛你那麽敏感，雖然是有些地方顯得生疏，但又很主動，我就覺得你就算有經驗也不奇怪」
艾特也不是什麽老司機，所以無法辨别。
「那只是因為，我懂這方面的知識而已啊。我在收集情報，還有尋寶時也見過做這種事的人」
「要是早告訴我你是第壹次，我還想着多讓你放松放松呢」
「那很難為情呀，而且我不想被你小瞧」
「你真是個笨蛋啊」
「......要你管」
哧溜，芭妮拉吸了下鼻涕。
「那然後呢，怎麽辦？你能自己動嗎？」
「當然。看我讓你爽到升天」
芭妮拉依照她的宣言動了起來，但似乎還是有些害怕。
她緩慢地上下擺動着腰肢。
「嗯......哈啊......艾特的......在肚子裏，動來動去......呀啊，啊啊......❤」
好像相當有感覺的樣子，她發出的聲音漸漸激烈了起來。
「啊，哈嗚......感覺，好舒服啊。艾特的，竟然會這麽舒服。都讓我有點後悔應該早點和你交合了。艾特呢？」
「那自然，是舒服了」
不愧是處女，夾得非常緊。
但是，并不會痛。
從左右擠壓過來的溫暖肉壁包裹住艾特，不斷給與他舒适的快感。
「太好了。我還擔心要是你說不舒服該怎麽辦呢」
「不過，這可才剛剛開始啊」
「诶？」
「我還沒開始動哦？正好也差不多習慣了，我也來動，讓你更爽。當然，我也會爽啦」
言畢，艾特動起了他的腰。
「——艾，艾特......！這個，好棒......摩擦着，好，舒服......！」
「你也配合我壹起動啊。而且，這還沒完呢」
「呀啊啊嗯❤」
芭妮拉之所以發出甜美的嬌喘，是因為艾特用力抓住了她的胸部。
雖不及莉莉希娅那麽大，但形狀優美的胸部。
艾特用舌尖挑逗起那上面膨脹的前端。
「你的乳頭真的好敏感啊」
「你明知道，還這麽做......！」
「這麽做，你那裏的反應會很好玩啊」
「死鬼，欺負人——」
她再次索吻。
于是艾特将嘴唇疊上去，伸出舌頭纏綿。
舌頭纏綿發出的啾啾聲，陰部發出的水聲，還有肌膚和肌膚相撞發出的啪啪聲漸漸充滿了浴室。
艾特的吐息，以及芭妮拉發出的嬌喘自然也包括在内。
「啊，不行......！我已經......！」
應該快到極限了吧。芭妮拉壹邊發出甜美的聲音，壹邊緊緊抱了過來。
「哎，好棒......！太舒服了，我都，變得不對勁了......呀，啊啊啊......❤」
此時，艾特注意到芭妮拉的脖子上出現了斑紋。
雖然位置和莉莉希娅相反，但大小差不多。
這就證明，她已經相當信任艾特了。
從斑紋發出了強光來看，她應該即将高潮，陷入了失控狀態吧。
嘴角壹邊耷拉着口水，芭妮拉繼續說道。
「......我已經，不行了......！腦袋裏，壹片空白，啊啊，呀啊嗚嗚......！」
下半身被用力夾着，艾特也迎來了極限。
「——唔，我要射了，芭妮拉！！」
仿佛決堤壹般，艾特射出了壹大股熾熱。
「啊，來了......！艾特滾燙的東西，射到了深處——啊啊啊啊......❤」
芭妮拉壹抖壹抖地迎來了高潮。
二人呼呼地喘着熾熱的吐息，就這麽維持了壹會兒互相擁抱的姿勢。
似乎是得到滿足，芭妮拉身上的斑紋已經消失了。
「你還好吧？」
艾特問向癱倒在懷裏的芭妮拉。
随後芭妮拉便擡頭答道。
「嗯。太舒服了，弄得我腦子裏只是壹片空白。艾特呢？舒服嗎？」
「這是當然」
「那，再吻我壹下❤」
芭妮拉閉上眼睛索起了吻。
艾特将自己的唇印了上去。
啾啾，發出了這樣的聲響。
看來芭妮拉比莉莉希娅更喜歡接吻。
親了壹會兒移開嘴唇後，
「哎，艾特。機會難得，再來壹次吧——」
她露出淫糜的表情，如此懇求道。雖說身體已經累了，但這個行為足以讓艾特的下半身東山再起。
發現這件事的芭妮拉握住那裏，在他耳邊低語道。
「呵呵，這不是幹勁十足嘛❤不過，壹直待在這裏的話可能會有别人進來，現在頭也有點暈了。等回我的房間咱們再繼續吧」


☆  ☆  ☆


從浴場轉移到芭妮拉的房間後。
剛壹走進房間艾特和芭妮拉兩人便立馬開始接吻，然後直接倒在了床上。
當他們身體再度重合，互相感覺對方，正幹柴烈火的時候。
門突然咚地壹聲響了起來。
「艾特......！」
傳來的聲音很小。
但确實帶有威脅性的可怕聲音。
「你在裏面吧！快點開門！」
門再次咚咚地響了起來。
「呃......」
「開門吧」
于是艾特整理好睡衣，前去開門。
出現在門後的，自然是莉莉希娅。
她正穿着酷似芭妮拉現在身上穿的睡衣。
「讓開」
魯莽地闖進房間的莉莉希娅推開艾特，走向芭妮拉。
「這是怎麽了，壹臉吓人的表情」
「聲音都傳到隔壁去了哎」
「既然如此，那你也應該知道我們在做什麽吧？和那時說好的壹樣，我也成為艾特的眷屬了。還讓他破了處。這下，我就真真正正的和你壹樣了哦」
「這麽說，你當時對芭妮拉說的話，都是肺腑之言咯」
艾特被莉莉希娅兇狠狠地瞪了壹眼。
「什麽意思，艾特？那些話，難道是騙我的嗎？你是撒了謊讓我和你壹起洗澡，在那裏跟我契約，還奪走了我的處女？」
「不是，本來澡堂就是你自己擅自進來的啊......！而且，我是不是在撒謊，你應該也已經很清楚了吧」
「我當然知道啊。我只是想向莉莉希娅炫耀壹下和你壹起洗了澡罷了。還有——」
芭妮拉露出微笑，将手放在子宮附近，
「也順便要向她展示壹下，這裏接受了艾特濃厚的愛，能十分清楚地感受到自己被愛的這件事呢❤」
「——芭，芭妮拉，你......！」
「哎呀，吃醋啦？」
呵呵，芭妮拉這麽笑了笑。
「才，才沒有呢，我才沒在吃醋......！」
「那你是想怎樣？如果沒事的話，你也不會過來吧。難道不是來制止我和艾特的？還是說，想壹起加入？」
「加入是——」
「你不願意的話也沒關系哦。反正我接下來打算再來壹次——不對，是要和艾特壹起交合到天亮呢❤然後把艾特迷得神魂颠倒，讓他成為只屬于我的艾特」
「呃，雖說泡溫泉恢復了些體力，但這再怎麽說也——疼 ，疼疼疼！」
莉莉希娅從壹旁揪住了他的臉蛋。
「為什麽你是以還要和芭妮拉做為前提說話？」
「哎呀，你現在想加入了嗎？」
「——被，被你那麽挑釁，我怎麽可能夾着尾巴逃走呢」
「死鴨子嘴硬，明明是自己想做」
「我想要做的，是和你決個高下」
「高下？」
「來比壹比誰能讓艾特更舒服」
這似乎是莉莉希娅為了說服自己而做出的妥協。
「原來如此。正合我意」
「——呃，咦咦？」
艾特完全被排擠在外。
但是，兩人之間似乎已經得出了結論。
「艾特，做好覺悟吧❤」
「沒錯，做好覺悟吧❤」
莉莉希娅與芭妮拉話音剛落，她們的胸部上面便分别浮現出發光的斑紋。
接着，艾特被兩人給推倒了。
三人在床上——。
開始了王與兩名眷屬之間的，熾熱的夜晚。


☆  ☆  ☆


「艾特，怎麽樣？還是我更舒服吧？」
「你說什麽呢？是我更舒服才對吧？對不對，艾特？」
「呃，呃......」
向下看去，能看到開始服侍自己的芭妮拉與莉莉希娅的臉。
二人正壹邊發出噗啾噗啾的淫糜水聲，壹邊呼出灼熱的吐息。
「呃，舒服倒是很舒服啦，但是......」
「但是，但是什麽啊？」
左手邊的莉莉希娅，仰視着瞪了過來。
「你該不會是想說，決定不了哪邊更舒服吧」
右手邊的芭妮拉也同樣仰視着瞪了過來。
「那種事。是絕對不可能的。因為艾特最喜歡這麽做了。對吧，艾特？」
先出現變化的是莉莉希娅。她将露出的前端從芭妮拉那裏搶走，壹口含了下去。
【插】
「啊，你耍賴！」
「嗯呵呵，你就先在這裏看着吧❤」
露出壹副成功拿下的表情的莉莉希娅用舌頭纏上來，邊溫暖着那裏邊侍奉了起來。
因為她很清楚，這麽做艾特會很有感覺。
然而，芭妮拉也不服輸。
「我才不呢，怎麽可能只是看着。既然你這麽來，那我就這樣」
撅起嘴尖後，芭妮拉鉆到艾特的屁股下面，握住垂在那裏的兩顆丸子又揉又舔。
「怎麽樣，艾特？這樣也很舒服吧？」
「這個嘛......」
确實很舒服。
看到艾特的反應，莉莉希娅似乎也覺得不能認輸。
「那麽這樣如何，艾特？」
她滋滋滋地吸起了肉棒。
以此帶來的直充腦門的刺激，在艾特的後背遊走起來。
看到艾特這個反應，莉莉希娅露出了滿意的笑容。
「呵呵，看來相當有感覺呢。那麽，我就這樣——」
「不行！在那之前先換我來！不然的話，就不算比賽了吧！」
「唉，真拿你沒辦法。那就交換吧。不過呢，艾特。你可絕對不能射哦。要忍到我來的時候才能射！」
你對我說這些也沒用啊......他會這麽想，是因為曾被芭妮拉的侍奉搞出來過。
「那麽，我要開始咯❤」
在迫不及待似地這麽說完之後。芭妮拉便想當然地用同樣的舌技展開了進攻。
「嗯啾，舔......怎麽樣？比起莉莉希娅，還是我做得更舒服吧？」
芭妮拉就像方才莉莉希娅做得壹樣，壹口把前端吞了下去。接着，莉莉希娅也用舌頭舔起了丸子。
用這種方法來舔對芭妮拉而言肯定也是第壹次，但應該是觀察過莉莉希娅是如何做得了吧。她做得相當有模有樣，很娴熟。
（啊啊——爽爆了，這畫面針不戳）
壹邊看着侍奉自己的二人壹邊承受着刺激後，腦袋裏就變得壹片空白。
照這麽下去，可能馬上就要射了。
是察覺到艾特這種狀態了嗎。
莉莉希娅瞪向芭妮拉，
「喂，你舔得時間應該和我壹樣了吧？已經夠了吧？」
「怎麽？你想說要由你來讓艾特射出來嗎？」
「......我，我不是這個意思」
「啊哈，原來如此」
芭妮拉好像是注意到了莉莉希娅的下半身正扭扭捏捏的。
「是你身為女人的部分奇癢難耐了對吧」
「等，芭妮拉——！」
「嗚呵呵，我懂我懂。我已經做過壹次了，不過莉莉希娅還沒做呢。那把勝負留到這邊再比我也無所謂......莉莉希娅呢？」
「嗚嗚......」
對于芭妮拉的提議，莉莉希娅并沒有異議。
二人的腦袋離開了艾特的下半身。
接着芭妮拉靠向艾特，貼到他的左耳邊——。
「所以呢，艾特。你插進來，評判壹下我們誰的裏面更舒服吧❤」
伸出舌頭舔了壹下耳朵裏面，令人發癢地這麽說道。
「我是絕對不會輸的......！」
莉莉希娅從右側拽住了艾特的胳膊。
「......唔，那好吧」
拒絕當然是不可能拒絕的。雄雄燃起的欲望在上湧，下半身的某部分已然變得更硬，更大。
「那你們兩個，先用手扶着那裏」
艾特看向墻壁。
想讓二人的屁股排成壹排的欲望，使得他這麽要求道。
「艾特，你是認真的嗎？」
這也太難為情了，莉莉希娅說道。
「只要是艾特的請求，我照單全收哦？」
聽話地将手扶在墻上的芭妮拉晃動起屁股，渴求着艾特。
「來，莉莉希娅似乎不喜歡，那就趕快再插到我裏面來吧」
「才沒那回事呢！我也——！」
繼芭妮拉之後，莉莉希娅也将手扶在墻上。
她壹臉害羞的表情，
「這樣......就行了吧？」
「啊啊，真是絕景」
看着眼前排成壹排的兩個屁股，艾特露出了賊笑。
就跟剛剛侍奉自己時壹樣——。
不，可能比那還贊。
莉莉希娅的屁股小巧又緊致，芭妮拉的屁股則稍大壹些。
也就是所謂的安產型吧。
每個都是充滿魅力的屁股，實在是幅美景。
在欣賞美景時，艾特發現了壹件事。
「都濕得壹塌糊塗了啊」
「咦，啊......」
将手伸進莉莉希娅的雙腿之間後，手便沾上了壹層蜜汁。
這只能代表她已經準備萬全了。
「你真是好色啊」
「煩，煩死了。我會變成這樣，還不全都怪你」
「什麽？莉莉希娅已經和艾特身經百戰到光是覺得期待，下面就洪水泛濫了嗎？」
「我，我們才沒做那麽多呢！壹只手都數得過來——」
「嘿......然後你就已經這樣了嗎，這不就代表，你本性淫亂嗎」
「什麽淫亂啊，你......！」
「算了，反正我是不會輸給你的。最能讓艾特舒服的，是我」
「你們兩個，别吵架啊」
艾特制止了互相對視，開始吹腔舌戰的二人。他壹口氣插入了莉莉希娅那耷拉着下流口水的私處。
「呀啊，進來了......❤」

(圖片014)

「啊，你耍賴！」
「......我，我耍什麽賴了。你不都已經做過壹次了嗎。所以我先是應該的」
「是啊，她說得沒錯。作為代替，芭妮拉就先靠這個湊合壹下吧」
「呀嗯❤」
艾特将中指與食指兩根指頭插進了芭妮拉體内。
他壹邊啪啪啪地用腰撞擊莉莉希娅的屁股，壹邊用手指攪來攪去。
「呀啊，裏面，被頂來頂去的......」
「用手指你都能這麽有感覺嗎」
「等，嗯嗯......艾特，這麽激烈......❤」
似乎因為剛高潮過的關系，現在相當敏感。芭妮拉壹邊晃着屁股，壹邊發出可愛的呻吟。
「還有，感覺莉莉希娅也比平時夾得更緊啊。被芭妮拉看到自己被幹的樣子，你性奮起來了？」
「才，才不是呢，我只是不想輸給芭妮拉而已！」
肌膚與肌膚啪啪啪地撞擊音，加上手指攪拌着私處發出的噗啾噗啾聲。以及二人的嬌喘，都漸漸充滿了這個房間。
正在繼續鼓掌的時候，艾特注意到從芭妮拉的小洞洞裏流出的東西。
那是他射出來的精華，和從芭妮拉的深處流出的蜜汁混雜在壹起的白濁液體。
看到那個，就令他忍不住想堵上。
「那麽，接下來換芭妮拉」
「咦，那我呢？」
拔出來的同時，莉莉希娅露出壹副苦悶的表情，表示自己還沒有高潮。
「馬上就會給你的。所以，就讓我先花心壹會兒吧」
「啊，艾特的......來，來了......！」
正如芭妮拉所言，艾特立馬插了進去。
接着便像是高興不已壹般，裏面用力地夾了過來。
就這麽做了壹會兒打樁運動後。
來到床上的艾特，來回品嘗着芭妮拉與莉莉希娅的身體。
沒被插的人，就和她接吻，揉她胸部。
舔她的乳頭，讓她舔自己的乳頭。
被年輕女孩特有的甘甜芳香包圍，真的是壹段無比幸福的時光。
但是，已經快到極限了。
莉莉希娅和芭妮拉二人應該也壹樣。
她們胸部上的斑紋正放出耀眼的光芒。
二人扭頭問道。
「哎，艾特。你會射在我裏面對吧？」
「說什麽呢！是要直接射在我裏面！」
現在正在幹的是芭妮拉。
該射給誰，他早已決定。
艾特從芭妮拉體内抽出，
「要射了，莉莉希娅！」
「喂，為什麽是莉莉希娅啊！」
莉莉希娅被插入後，芭妮拉靠了過來。
她壹臉不滿地責問着為什麽。
「我已經射給你壹次了啊。這是為了公平起見。不過，我會讓你高潮的，你先靠這個忍壹下」
啾噗，艾特又将手指插入了芭妮拉體内。接着又吻向她，加快了手指的動作。
當然莉莉希娅依然在被幹着，他也加快了腰部的動作。
「來吧，你們壹起去吧！」
「讨厭，我，要被艾特的手指，弄高潮了......！啊，啊啊啊啊......！！」
噗呲壹聲，芭妮拉的下半身噴出了潮水。
同樣地，莉莉希娅似乎也迎來了極限。
「我，我也......要，不行了......。艾特，快點給我！在我裏面，把滾燙的......啊，呀啊，呀啊啊啊......！！」
她不斷顫抖着身體，仿佛渴望着艾特的東西壹樣，裏面用力夾了過來。
這讓艾特也到達了極限。
「啊，來了......！艾特滾燙的，射到了我的深處......啊，好熱，我，要去了......去了，啊啊啊啊啊啊啊......❤」
艾特滾燙的欲望熱流，不斷在莉莉希娅體内釋放了出來。腦袋被快樂支配變得壹片空白，然後他便無力地癱倒在床上。
莉莉希娅和芭妮拉也壹樣。
二人胸上的斑紋已經消失了。
三人仰躺着排成了川字形。
他們維持着這個姿勢，調整了壹會兒紊亂的呼吸。
接着最先動起來的是芭妮拉。
她半翻着身體抓住艾特的胳膊，
「哎，艾特。你還射得出來嗎？還可以的話，就在我裏面射壹次——」
「你在說什麽呢？如果你還要再來壹次的話，那我可也要再來壹次哦？」
莉莉希娅在芭妮拉的相反側，同樣抱住了他的胳膊。
「嗯呵呵，看你現在多老實❤」
「要，要你多嘴！」
艾特不禁苦笑，
「今天，就當你倆平手吧」
他真的已經動彈不得了。
到極限了。
（不過，努力還是值得的）
結果就是他現在左擁右抱。
真的就像是王壹樣，艾特抱着兩名少女柔軟的身體，壹邊滿足于那股溫暖，壹邊沉沉睡去。


☆  ☆  ☆


醒來後天已經亮了。
等待着将壹絲不挂還在夢鄉中的莉莉希娅和芭妮拉留下返回自己房間的艾特的，是在床上露出賊笑的維諾斯。
「真是玩得夠嗨啊」
「我們那麽搞，你也很滿足吧？」
「嗯，這裏感受到了非常多的愛呢。我也好幾次差點去了呢」
走下床的維諾斯壹邊開心地亮出下腹部發光的紋章，壹邊露出淫糜的微笑。
「梅洛蒂沒生氣吧？」
「在回到房間的同時，她就已經累得睡着了。根本顧不上生氣啦」
「是嗎」
「不過話說回來，你還真是能幹呢。和芭妮拉締結眷屬契約雖然是在預料之内的事，可沒想到竟然連莉莉希娅也壹起加入，搞起3P來了。我都想誇誇你真得很有王者風範了。當然，還是希望你能再接再厲」
「你是說，讓我建立後宮嗎？」
「是啊。應該會變成這樣吧」
在床上躺下，他仰望着天花板思考起來。
昨晚真是爽爆了。
要是再讓更多美少女來侍奉自己——。
（不錯啊）
光是想象就讓人忍俊不禁，興奮起來了。
被眾多美少女環繞，正可謂有種王者的感覺。
肯定能比昨晚更加快樂吧。
此時壹道聲音，瞬間打碎了他腦袋裏浮現的這些妄想。
「......後宮？」
隔壁床上的梅洛蒂坐了起來。
她用力伸了個懶腰，擦了擦眼睛。
「哥哥，後宮是什麽？」
「呃......」
「就是增加家人啊」
正當艾特想着該怎麽搪塞過去時，維諾斯這麽答道。
「家人！」
梅洛蒂雙眼放光。
「家人能增加，你也很開心吧？」
「嗯！梅洛蒂會很開心的！」
是因為待在壹起的時間變長了嗎，維諾斯現在相當會應付梅洛蒂了。
不管怎麽說只要她能接受就好，在他這麽想的時候響起了敲門聲。
「艾特，你醒了嗎？」
傳來了莉莉希娅的聲音。
看來她似乎醒了。
「噢」
他坐起身，向門外回應道。
「門開着呢，進來吧」
言畢，莉莉希娅打開門走了進來。
芭妮拉也跟着她進來了。
「大家都醒了呀」
環視着屋内，莉莉希娅這麽說道。
「剛剛，正在和哥哥聊天呢」
梅洛蒂如此回答。
「聊天？」
「嗯，聊增加家人的事！」
随後莉莉希娅的臉便壹口氣紅了起來，
「什！？我說你，都對梅洛蒂妹妹說了點什麽啊！」
「慢着，那是維諾斯她——」
「但是繼續重復昨晚的事，不就會真的增加家人嗎」
「芭妮拉，你又在說什麽呢......！」
朝芭妮拉看過去，她正用手捂着下腹部。
剛好是在子宮的位置。
會孕育小寶寶的地方。
「說起來，我還沒告訴過你來着？若是眷屬與主人的話，是生不了孩子的哦」
「诶，是嗎！？」
「成為艾特眷屬的人會在子宮内展開結界，将精子作為魔力吸收掉的。我的魔力就是靠這個來恢復的——」
「喂，你們先給我閉嘴！梅洛蒂還在這兒呢。而且，我們可沒在談這種事吧」
「哈哈哈，說得也對。我們在聊得是增加艾特的眷屬，建造後宮這件事呢」
「啊啊，原來是這樣——喂，這件事我可不能當耳旁風啊」
莉莉希娅狠狠地瞪了過來。
接着，芭妮拉接過了話茬。
「可是，想要把國家從帝國手裏奪回來，咱們還需要很多強大的夥伴吧？強大的夥伴變多了，對你來說不也是好事嗎？」
「呃，話是這麽說沒錯......」
「嗚呵呵，該不會，又吃醋了？」
「你，你好煩欸！啊啊夠了，為什麽會變成這樣啊......」
莉莉希娅雙手抱頭蹲了下去。壹邊覺得這樣的她很可愛，艾特壹邊為了改變話題，拿起了放在桌子上的地圖。
「不管怎麽說，現在我們首要任務，是前往下個目的地」
「啊，這個，是我昨天搶來的對吧」
芭妮拉說得沒錯，這是她昨天從倒下的盧法斯身上奪來的地圖。除了昨天那座神殿之外，還标記了其他幾座遺迹。
「這些，都代表這裏被封印着古魔女對吧？離這裏最近的是......這裏吧」
探頭從旁看着地圖的莉莉希娅所指向的，是距離巴努亞斯鎮最近的畫着X标記的地方。
接着，莉莉希娅問道。
「維諾斯，這個地方你有什麽印象嗎？」
「昨晚艾特已經問過我了，像是有，又好像沒有......。之前我說過，那都已經是很久之前的事了，記憶很模糊。我體内的阿莎蕾娅似乎也壹樣」
所以他們之前讨論得出的結論就是只能去跑壹趟才知道，如今依舊是這個結論。畢竟過去看看，說不定她會想起什麽。
搶在帝國前面，确保能夠成為同伴的古魔女以及危險的古魔女。
視情況而定，可以像阿莎蕾娅那樣讓她化為維諾斯的力量。
維諾斯似乎可以和自己體内的阿莎蕾娅對話，知識量好像也增加了。
不管怎麽說艾特他們若想赢過帝國的話就只有這條路可走，盧法斯他們也為了确保古魔女而展開行動了。
雖然不知道帝國皇帝多這些情況了解多少，但無法摒除古魔女的事已經傳入他耳中的可能性。
與古魔女契約已經是件可行的事了。
況且也有說法是原本那些建國者們都和古魔女契約過，所以就算皇帝想要尋求這份力量，打算去尋找，并将王之力據為己有也不奇怪。
話雖如此，但無論如何他們要做的事都是不變的。
盡可能地得到古魔女協助——或者得到她們的力量。
盡可能地，獲得志同道合想要打倒帝國的同伴。
然後，将許多可愛的女孩子收為眷屬，建立後宮。
這麽做也與最終目的所挂鉤，當然更不會產生影響。
「......艾特，你是不是在想些下流的事」
「沒啊，我只是在考慮下壹步該怎麽做而已啊」
「那你所謂要做的事，到底指的是什麽。看你嘴巴都笑歪了」
「這，這個嘛，當然是尋找古魔女和夥伴咯」
「還有建立後宮，對吧❤」
就像是能讀艾特的心壹樣，芭妮拉這麽說道。她抱住艾特的左臂，緊緊地貼了過來。
「我呢，就算當後宮的壹員也沒問題哦。反正艾特說過會永遠陪着我。這樣我就已經滿足了。那邊的某個人似乎不是呢」
「才沒有呢！只是如果艾特要稱王的話，作為公主的我就得是正妻而已。不管他是要建立後宮還是什麽的都壹樣！」
和芭妮拉形成對抗般，莉莉希娅抱住了右臂。
「感覺好好玩！梅洛蒂也要加入！」
「等，怎麽連梅洛蒂也......！」
繼左右分别抱過來的芭妮拉和莉莉希娅之後，梅洛蒂又從正面抱了上來。
接着她仰着頭，
「因為，哥哥昨天晚上沒有陪我玩嘛」
「哈哈，說來也是......」
看着被三名女性包圍的艾特，維諾斯咯咯地笑了起來。
「啊哈哈，真不錯。就好像真的壹家人壹樣呢。也加我壹個吧」


這是壹名帝國對底層的青年，與古魔女相遇，得到魔眼，成功以下克上的故事。
是由得到王之力的艾特所書寫的，在全世界奔走，獲得新的家人的故事。

（完）